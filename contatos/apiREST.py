from flask import Flask
from flask import request
from flask import make_response
from flask import render_template
from recursos import *
from dados import *
app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False

dados = Dados()
joao = dados.criar_pessoa("Joao da Silva")
joao_whatsapp = dados.criar_contato(joao, "WhatsApp", "489999889292")


@app.route('/', methods=['GET'])
def recurso_raiz():
	resposta = make_response("")
	resposta.headers['Location'] = '/pessoas'
	return resposta, 307

@app.route('/pessoas', methods=['GET', 'POST'])
def recurso_pessoas():
	return RecursoPessoas(dados).tratar_requisicao(request)

@app.route('/pessoas/<int:identificadorDaPessoa>', methods=['GET', 'PUT', 'DELETE', 'POST'])
def recurso_uma_pessoa(identificadorDaPessoa):
	return RecursoPessoa(dados).tratar_requisicao(request, {'identificadorDaPessoa': identificadorDaPessoa})

@app.route('/pessoas/<int:identificadorDaPessoa>/contatos', methods=['GET', 'POST'])
def recurso_contatos(identificadorDaPessoa):
	return RecursoContatos(dados).tratar_requisicao(request, {'identificadorDaPessoa': identificadorDaPessoa})

@app.route('/pessoas/<int:identificadorDaPessoa>/contatos/<int:identificadorDoContato>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def recurso_um_contato_de_uma_pessoa(identificadorDaPessoa, identificadorDoContato):
	return RecursoContato(dados).tratar_requisicao(request, {'identificadorDaPessoa': identificadorDaPessoa, 'identificadorDoContato': identificadorDoContato})