from flask import render_template
from flask import make_response
from dados import *

class Recurso:

	def tratar_requisicao(self, requisicao, parametros = {}):
		if requisicao.method == 'GET':
			return self.tratar_get(requisicao, parametros)
		elif ((requisicao.method == 'PUT') or ('_method' in requisicao.args and requisicao.args['_method'] == 'put' and requisicao.method == 'POST')):
			return self.tratar_put(requisicao, parametros)
		elif ((requisicao.method == 'DELETE') or ('_method' in requisicao.args and requisicao.args['_method'] == 'delete' and requisicao.method == 'POST')):
			return self.tratar_delete(requisicao, parametros)
		elif requisicao.method == 'POST':
			return self.tratar_post(requisicao, parametros)
		print metodo
		return 'OPS', 500

	def tratar_get(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_put(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_post(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_delete(self, requisicao, parametros):
		return "Metodo nao implementado", 501

class RecursoPessoas(Recurso):

	def __init__(self, dados):
		self.dados = dados

	def tratar_get(self, requisicao, parametros):
		return render_template("pessoas.html", pessoas=self.dados.obterPessoas()), 200

	def tratar_post(self, requisicao, parametros):
		if (requisicao.form['nome']):
			pessoa = self.dados.criar_pessoa(requisicao.form['nome'])
			resposta = make_response("")
			resposta.headers['Location'] = '/pessoas/{0}'.format(pessoa.obter_identificador())
			return resposta, 303
		else:
			erros['nome'] = True
			return render_template("pessoas.html", pessoas=self.dados.obterPessoas(), formulario=requisicao.form, erros=erros), 400
		return render_template("pessoaNaoEncontrada.html"), 404


class RecursoPessoa(Recurso):

	def __init__(self, dados):
		self.dados = dados

	def tratar_get(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (isinstance(pessoa, Pessoa)):
			return render_template("pessoa.html", pessoa=pessoa)
		return render_template("pessoaNaoEncontrada.html"), 404

	def tratar_put(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (isinstance(pessoa, Pessoa)):
			if (requisicao.form['nome']):
				pessoa.fixar_nome(requisicao.form['nome'])
				self.dados.atualizar_pessoa(pessoa)
				return render_template("pessoa.html", pessoa=pessoa), 200
			else:
				erros['nome'] = True
				return render_template("pessoa.html", pessoa=pessoa, erros=erros), 400
		return "Pessoa nao encontrada", 404

	def tratar_delete(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (isinstance(pessoa, Pessoa)):
			self.dados.removerPessoa(identificadorDaPessoa)
			return render_template("pessoaRemovida.html", pessoa=pessoa), 200
		return render_template("pessoaNaoEncontrada.html"), 404


class RecursoContatos(Recurso):

	def __init__(self, dados):
		self.dados = dados

	def tratar_get(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (isinstance(pessoa, Pessoa)):
			return render_template("contatosDePessoa.html", pessoa=pessoa, contatos=self.dados.obter_contatos(pessoa)), 200
		return render_template("pessoaNaoEncontrada.html"), 404

	def tratar_post(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (isinstance(pessoa, Pessoa)):
			if (requisicao.form['descricao'] and requisicao.form['valor']):
				contato = self.dados.criar_contato(pessoa, requisicao.form['descricao'], requisicao.form['valor'])
				resposta = make_response("")
				resposta.headers['Location'] = '/pessoas/{0}/contatos/{1}'.format(pessoa.obter_identificador(), contato.obter_identificador())
				return resposta, 303
			else:
				erros = {}
				if not requisicao.form['descricao']:
					erros['descricao'] = True
				if not requisicao.form['valor']:
					erros['valor'] = True
				return render_template("contatosDePessoa.html", pessoa=pessoa, contatos=self.dados.obterContatos(pessoa), formulario=requisicao.form, erros=erros), 400
		return render_template("pessoaNaoEncontrada.html"), 404

class RecursoContato(Recurso):

	def __init__(self, dados):
		self.dados = dados

	def tratar_get(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		identificadorDoContato = parametros['identificadorDoContato']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		contato = self.dados.obter_contato(pessoa, identificadorDoContato)
		if (isinstance(contato, Contato)):
			return render_template("contato.html", pessoa=pessoa, contato=contato), 200
		return render_template("contatoNaoEncontrado.html", pessoa=pessoa, contato=contato), 404

	def tratar_put(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		identificadorDoContato = parametros['identificadorDoContato']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		if (requisicao.form['descricao'] and requisicao.form['valor']):
			contato	= self.dados.obter_contato(pessoa, identificadorDoContato)
			if (isinstance(contato, Contato)):
				contato.fixar_descricao(requisicao.form['descricao'])
				contato.fixar_valor(requisicao.form['valor'])
				self.dados.atualizar_contato(pessoa, contato)
				return render_template("contato.html", pessoa=pessoa, contato=contato), 200
		return render_template("contatoNaoEncontrado.html", pessoa=pessoa, contato=contato), 404

	def tratar_delete(self, requisicao, parametros):
		identificadorDaPessoa = parametros['identificadorDaPessoa']
		identificadorDoContato = parametros['identificadorDoContato']
		pessoa = self.dados.obterPessoa(identificadorDaPessoa)
		contato	= self.dados.obter_contato(pessoa, identificadorDoContato)
		if (isinstance(pessoa, Pessoa) and isinstance(contato, Contato)):
			self.dados.remover_contato(pessoa, contato)
			return render_template("contatoRemovido.html", pessoa=pessoa, contato=contato), 200
		return render_template("contatoNaoEncontrado.html", pessoa=pessoa, contato=contato), 404