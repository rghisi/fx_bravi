import sys

class ValidadorDeEntrada:

	def __init__(self):
		self.paresDeCaracteres = {'{': '}', '[':']', '(':')'}
		self.caracteresDeAbertura = set(self.paresDeCaracteres.keys())
		self.caracteresDeFechamento = set(self.paresDeCaracteres.values())
		self.caracteresValidos = set(self.paresDeCaracteres.keys())
		self.caracteresValidos.update(self.paresDeCaracteres.values())

	def validar(self, texto):
		pilhaDeContextos = []
		for caractere in texto:
			if (caractere in self.caracteresValidos):
				if (caractere in self.caracteresDeAbertura):
					pilhaDeContextos.append(caractere)
				else:
					caractereAnterior = pilhaDeContextos.pop()
					if (self.paresDeCaracteres[caractereAnterior] != caractere):
						return False
			else:
				return False
		return len(pilhaDeContextos) == 0

while (True):
	try:
		entrada = raw_input("Entre com a sequencia de caracteres que deseja validar: ")
		validadorDeEntrada = ValidadorDeEntrada()
		print entrada, validadorDeEntrada.validar(entrada)
	except KeyboardInterrupt:
		print ""
		sys.exit()