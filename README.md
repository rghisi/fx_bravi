# Teste FX/Bravi

## Conteúdo:

* Contextos - analisador de contextos balanceados
* Contatos - API REST para adicionar/remover/alterar pessoas e seus contatos
* Condição do Tempo - Aplicação WEB para obter a condição do tempo atual em uma cidade

## Contextos

Como rodar:

1. `$ cd contextos`
2. `contextos$ python contextos.py`
3. Entrar com a sequência desejada e pressionar `Enter`
4. Para sair, utilizar `CTRL + C`

Pré-requisitos:

* Python 2


## Contatos
Como rodar:

1. `$ cd contatos`
2. `contatos$ ./rodar.sh`
3. Acessar 'http://localhost:5000/` de um navegador
4. O uso da API pode ser feito pelo navegador, auto-explicativo

Pré-requisitos:

* Python 2
* Flask

Problemas conhecidos:

  *  Não suporta caracteres com acentos (UTF-8)

## Condição do Tempo
Como rodar:
1. `$ cd condicaoDoTempo`
2. `condicaoDoTempo$ ./rodar.sh`
3. Acessar 'http://localhost:5000/` de um navegador

Pré-requisitos:

* Python 2
* Flask

Problemas conhecidos:

  *  Não suporta caracteres com acentos
