import requests
import os
import json
from datetime import datetime

class CondicaoDeTempoOpenWeatherMap:

	FORMATO_DATA_HORA = "%d/%m/%Y %H:%M:%S"

	def __init__(self, condicao_de_tempo_json):
		self.cidade = condicao_de_tempo_json["name"]
		self.pais = condicao_de_tempo_json["sys"]["country"]
		self.nascer_do_sol = datetime.fromtimestamp(condicao_de_tempo_json["sys"]["sunrise"]).strftime(CondicaoDeTempoOpenWeatherMap.FORMATO_DATA_HORA)
		self.por_do_sol = datetime.fromtimestamp(condicao_de_tempo_json["sys"]["sunset"]).strftime(CondicaoDeTempoOpenWeatherMap.FORMATO_DATA_HORA)
		self.temperatura_atual = condicao_de_tempo_json["main"]["temp"]
		self.temperatura_maxima = condicao_de_tempo_json["main"]["temp_max"]
		self.temperatura_minima = condicao_de_tempo_json["main"]["temp_min"]
		self.umidade = condicao_de_tempo_json["main"]["humidity"]
		self.pressao = condicao_de_tempo_json["main"]["pressure"]
		self.velocidade_do_vento = float(condicao_de_tempo_json["wind"]["speed"])
		self.direcao_do_vento = self.__obter_direcao_do_vento_textual(float(condicao_de_tempo_json["wind"]["deg"]))
		self.nebulosidade = condicao_de_tempo_json["clouds"]["all"]
		if "rain" in condicao_de_tempo_json:
			self.volume_de_chuva = condicao_de_tempo_json["rain"]["3h"]
		else:
			self.volume_de_chuva = 0
		if "snow" in condicao_de_tempo_json:
			self.volume_de_neve = condicao_de_tempo_json["snow"]["3h"]
		else:
			self.volume_de_neve = 0
		self.condicao_geral = condicao_de_tempo_json["weather"][0]["description"][:1].upper() + condicao_de_tempo_json["weather"][0]["description"][1:]
		self.icone_condicao_geral = condicao_de_tempo_json["weather"][0]["icon"]
		self.instante_da_atualizacao = datetime.fromtimestamp(condicao_de_tempo_json["dt"]).strftime(CondicaoDeTempoOpenWeatherMap.FORMATO_DATA_HORA)

	def __obter_direcao_do_vento_textual(self, direcao_do_vento_em_graus):
		if direcao_do_vento_em_graus >= 337.5 or direcao_do_vento_em_graus < 22.5:
			return "N"
		elif direcao_do_vento_em_graus >= 22.5 and direcao_do_vento_em_graus < 67.5:
			return "NE"
		elif direcao_do_vento_em_graus >= 67.5 and direcao_do_vento_em_graus < 112.5:
			return "E"
		elif direcao_do_vento_em_graus >= 112.5 and direcao_do_vento_em_graus < 157.5:
			return "SE"
		elif direcao_do_vento_em_graus >= 157.5 and direcao_do_vento_em_graus < 202.5:
			return "S"
		elif direcao_do_vento_em_graus >= 202.5 and direcao_do_vento_em_graus < 247.5:
			return "SW"
		elif direcao_do_vento_em_graus >= 247.5 and direcao_do_vento_em_graus < 292.5:							
			return "W"
		elif direcao_do_vento_em_graus >= 292.5 and direcao_do_vento_em_graus < 337.5:
			return "NW"
		return ""


class CondicaoDeTempoNaoEncontrada:

	def __init__(self, cidade):
		self.cidade = cidade		

class OpenWeatherMap:

	URI_ICONES = "http://openweathermap.org/img/w/{0}.png"

	URI_PREVISAO_DO_TEMPO = "http://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&units=metric&lang=pt"

	def __init__(self, chave_api):
		self.chave_api = chave_api

	def obter_condicao_atual(self, cidade):
		uri = OpenWeatherMap.URI_PREVISAO_DO_TEMPO.format(cidade, self.chave_api)
		resposta = requests.get(uri)
		if resposta.status_code == 200:
			return CondicaoDeTempoOpenWeatherMap(resposta.json())
		else:
			return CondicaoDeTempoNaoEncontrada(cidade)