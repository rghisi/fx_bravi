class Pessoa:

	def __init__(self, nome, identificador = 0):
		self.nome = nome
		self.identificador = identificador

	def obter_nome(self):
		return self.nome;

	def fixar_nome(self, novoNome):
		self.nome = novoNome

	def obter_identificador(self):
		return self.identificador

class PessoaNaoExistente:
	def obter_identificador(self):
		return -1

class Contato:
	
	def __init__(self, descricao, valor, identificador = 0):
		self.descricao = descricao
		self.valor = valor
		self.identificador = identificador

	def obter_descricao(self):
		return self.descricao;

	def fixar_descricao(self, novaDescricao):
		self.descricao = novaDescricao

	def obter_valor(self):
		return self.valor;

	def fixar_valor(self, novoValor):
		self.valor = novoValor

	def obter_identificador(self):
		return self.identificador

class ContatoNaoExistente:
	def obter_identificador(self):
		return -1


class Dados:
	
	def __init__(self):
		self.pessoas = []
		self.contatos = {}

	def criar_pessoa(self, nome):
		identificador = len(self.pessoas)
		novaPessoa = Pessoa(nome, identificador)
		self.pessoas.append(novaPessoa)
		return novaPessoa

	def atualizar_pessoa(self, pessoa):
		if (pessoa.obter_identificador() >= len(self.pessoas)):
			return PessoaNaoExistente()
		else:
			self.pessoas[pessoa.obter_identificador()] = pessoa
			return pessoa

	def removerPessoa(self, identificador):
		self.pessoas[identificador] = PessoaNaoExistente()

	def obterPessoa(self, identificador):
		if (identificador >= len(self.pessoas)):
			return PessoaNaoExistente()
		else:
			return self.pessoas[identificador]

	def obterPessoas(self):
		return self.pessoas

	def obter_contatos(self, pessoa):
		if (pessoa.obter_identificador() in self.contatos):
			contatosDaPessoa = self.contatos[pessoa.obter_identificador()]
		else:
			contatosDaPessoa = []
		return contatosDaPessoa

	def criar_contato(self, pessoa, descricao, valor):
		if (pessoa.obter_identificador() in self.contatos):
			contatosDaPessoa = self.contatos[pessoa.obter_identificador()]
		else:
			contatosDaPessoa = []
		novoContato = Contato(descricao, valor, len(contatosDaPessoa))
		contatosDaPessoa.append(novoContato)
		self.contatos[pessoa.obter_identificador()] = contatosDaPessoa
		return novoContato

	def obter_contato(self, pessoa, identificadorDoContato):
		if (pessoa.obter_identificador() in self.contatos):
			contatosDaPessoa = self.contatos[pessoa.obter_identificador()]
			if (identificadorDoContato < len(contatosDaPessoa)):
				return contatosDaPessoa[identificadorDoContato]
		return ContatoNaoExistente()

	def atualizar_contato(self, pessoa, contato):
		if (pessoa.obter_identificador() in self.contatos):
			contatosDaPessoa = self.contatos[pessoa.obter_identificador()]
			if (contatosDaPessoa[contato.obter_identificador()]):
				contatosDaPessoa[contato.obter_identificador()] = contato
				return contato
		return ContatoNaoExistente()

	def remover_contato(self, pessoa, contato):
		if (pessoa.obter_identificador() in self.contatos):
			contatosDaPessoa = self.contatos[pessoa.obter_identificador()]
			if (contatosDaPessoa[contato.obter_identificador()]):
				contatosDaPessoa[contato.obter_identificador()] = ContatoNaoExistente()
		return ContatoNaoExistente()
