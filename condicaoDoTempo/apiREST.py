from flask import Flask
from flask import request
from flask import make_response
from flask import render_template
from recursos import *
app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False

@app.route('/', methods=['GET'])
def recurso_raiz():
	resposta = make_response("")
	resposta.headers['Location'] = '/condicaoDoTempo'
	return resposta, 307

@app.route('/condicaoDoTempo', methods=['GET'])
def recurso_condicao_do_tempo():
	return RecursoCondicaoDoTempo().tratar_requisicao(request)

@app.route('/condicaoDoTempo/', methods=['GET'])
def recurso_condicao_do_tempo2():
	return RecursoCondicaoDoTempo().tratar_requisicao(request)

@app.route('/condicaoDoTempo/<string:cidade>', methods=['GET'])
def recurso_condicao_do_tempo_de_uma_cidade(cidade):
	return RecursoCondicaoDoTempoDeUmaCidade().tratar_requisicao(request, {'cidade': cidade})