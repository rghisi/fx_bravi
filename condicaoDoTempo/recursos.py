from flask import render_template
from flask import make_response
from clienteOpenWeatherMap import *

class Recurso:

	def tratar_requisicao(self, requisicao, parametros = {}):
		if requisicao.method == 'GET':
			return self.tratar_get(requisicao, parametros)
		elif ((requisicao.method == 'PUT') or ('_method' in requisicao.args and requisicao.args['_method'] == 'put' and requisicao.method == 'POST')):
			return self.tratar_put(requisicao, parametros)
		elif ((requisicao.method == 'DELETE') or ('_method' in requisicao.args and requisicao.args['_method'] == 'delete' and requisicao.method == 'POST')):
			return self.tratar_delete(requisicao, parametros)
		elif requisicao.method == 'POST':
			return self.tratar_post(requisicao, parametros)
		print metodo
		return 'OPS', 500

	def tratar_get(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_put(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_post(self, requisicao, parametros):
		return "Metodo nao implementado", 501

	def tratar_delete(self, requisicao, parametros):
		return "Metodo nao implementado", 501

class RecursoCondicaoDoTempo(Recurso):

	def tratar_get(self, requisicao, parametros):
		if requisicao.args.get('cidade'):
			resposta = make_response("")
			resposta.headers['Location'] = "/condicaoDoTempo/{0}".format(requisicao.args.get('cidade'))
			return resposta, 307
		else:
			return render_template("buscarCondicaoDoTempo.html"), 200

class RecursoCondicaoDoTempoDeUmaCidade(Recurso):

	def tratar_get(self, requisicao, parametros):
		openWeatherMap = OpenWeatherMap("6d58d90282f5dfc3e03d68c950deb983")
		cidade = parametros["cidade"]
		condicaoDoTempo = openWeatherMap.obter_condicao_atual(cidade)
		if isinstance(condicaoDoTempo, CondicaoDeTempoNaoEncontrada):
			return render_template("condicaoDoTempoDeUmaCidade_cidadeNaoEncontrada.html", condicaoDoTempo=condicaoDoTempo), 404
		else:
			return render_template("condicaoDoTempoDeUmaCidade.html", condicaoDoTempo=condicaoDoTempo), 200
